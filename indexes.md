Задание
=========

Дан запрос 

    SELECT * FROM some_table WHERE a AND b ORDER BY c
    
Напишите все возможные комбинации индексов, которые могут ускорить выполнение запроса.  
Где а, b, c — поля в таблице.

=======

Если `a` имеет меньше вариантов, чем `b`
или по `a` идет точный поиск

    create index ix_some_table_1 on some_table (a, b, c asc);

Если `b` имеет меньше вариантов, чем `a`
или по `b` идет точный поиск

    create index ix_some_table_1 on some_table (b, a, c asc);

Так же Postgres может использовать комбинированные индексы

    create index ix_some_table_1 on some_table (a, c asc);
    create index ix_some_table_1 on some_table (b, c asc);

Если `a` или `b` что то специфическое можно попробовать

    create index ix_some_table_1 on some_table (a, c asc) where b;

В зависимости от требований по уникальности могут быть созданы индексы 

    create unique index ix_some_table_1 on some_table (a)
    create unique index ix_some_table_1 on some_table (b)
    create unique index ix_some_table_1 on some_table (a, b)
