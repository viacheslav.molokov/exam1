/* 
 
 Дана таблица items (id, category_id, name, price).
 
 Напишите один запрос, без использования вложенных SELECT,
 который выбирает 3 товара с максимальной ценой из каждого category_id.
 */
-- Используется Postgresql
-- DDL
create table items (
    id SERIAL PRIMARY KEY,
    category_id integer,
    price integer,
    name text
);
create index pk_items on items (id);
create index ix_items_top on items (price desc, category_id);
-- Вставка данных
insert into items (category_id, price, name)
values (1, 1, 'C1P1'),
    (1, 2, 'C1P2'),
    (1, 3, 'C1P3'),
    (1, 4, 'C1P4'),
    (2, 21, 'C2P21'),
    (2, 22, 'C2P22'),
    (2, 23, 'C2P23'),
    (2, 24, 'C2P24'),
    (3, 31, 'C3P31'),
    (3, 32, 'C3P32'),
    (3, 33, 'C3P33'),
    (3, 34, 'C3P34'),
    (3, 31, 'C3P31'),
    (3, 32, 'C3P32'),
    (3, 33, 'C3P33'),
    (3, 34, 'C3P34');
-- Запрос
-- explain analyze
with categories1 as (
    select category_id as id
    from items
    group by category_id
)
select top_items.category_id,
    top_items.id,
    top_items.price,
    top_items.name
from categories1 i
    cross join lateral (
        select *
        from items ii
        where ii.category_id = i.id
        order by ii.price desc
        limit 3
    ) top_items
order by top_items.category_id,
    top_items.price desc;
-- При наличии таблицы с категориями товаров запрос можно оптимизировать
create table categories (id SERIAL PRIMARY KEY, name text);
insert into categories (name)
values ('C1'),
    ('C2'),
    ('C3');
-- explain analyze
select top_items.category_id,
    top_items.id,
    top_items.price,
    top_items.name
from categories i
    cross join lateral (
        select *
        from items ii
        where ii.category_id = i.id
        order by ii.price desc
        limit 3
    ) top_items
order by top_items.category_id,
    top_items.price desc;
/* PS: В зависимости от реальных потребностей бизнеса, может быть намного
 эффекитвнее использовать UNION с заранее известными категориями,
 чем насиловать базу выбирая из всех возможных категорий (которых может быть
 очень много) рандомные (для пользователя) 3 самых дорогих товара. 
 В большинстве случаев мы все равно покажем в итоге не более 50 - 100 вариантов.
 
 PS2: В качестве альтернативы и при редком изменении данных можно использовать материалзованное
 представление, которое предоставит результаты этим же запросом.
 
 PS3: Если мы делаем сервис по работе с товарами и в обход нас никто в базе ничего менять не будет,
 можно кэшировать результат этого запроа при запуске (или перебирать их UNION-ом группами) и обновлять кэш
 по мере обновления данных через API нашего сервиса. Для форс-мажоров и правок в базе напрямую можно
 реализовать сервис, который пересканирует одну или несколько категорий товаров и обновит их кэш.
 */