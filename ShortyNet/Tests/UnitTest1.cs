using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Tests {
    public class Tests {
        [SetUp]
        public void Setup() {
        }

        [Test]
        public async Task Test1() {
            var client = new HttpClient(new HttpClientHandler() {
                AllowAutoRedirect = false,
                
            });

            var res = await client.PostAsync("http://127.0.0.1:5000/api/link", new FormUrlEncodedContent(
                new List<KeyValuePair<string, string>>() {
                    new KeyValuePair<string, string>("url",  "https://gitlab.com/viacheslav.molokov/exam1")
                }));
            
            Assert.AreEqual(HttpStatusCode.Created, res.StatusCode);

            var location = res.Headers.Location;
            
            Assert.IsNotEmpty(location.ToString());
            var res2 = await client.GetAsync($"http://127.0.0.1:5000{location}");
            
            Assert.AreEqual(HttpStatusCode.Redirect, res2.StatusCode);
        }
        
        [Test]
        public async Task Test2() {
            var client = new HttpClient(new HttpClientHandler() {
                AllowAutoRedirect = false,
                
            });
            var location = "/../../bin/bash";
            var res2 = await client.GetAsync($"http://127.0.0.1:5000{location}");
            
            Assert.AreEqual(HttpStatusCode.NotFound, res2.StatusCode);
        }
    }
}