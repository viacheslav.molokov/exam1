using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ShortyNet.Services {
    class LinkId {
        private static readonly Random RandomGenerator = new Random();
        private static int _nextId = 0;
        private static readonly int MachineId = RandomGenerator.Next();

        private readonly long _timeCode;
        private readonly int _myId;
        private readonly int _randomSeed;

        public LinkId() {
            _timeCode = DateTime.UtcNow.Ticks;
            _myId = Interlocked.Increment(ref _nextId);
            _randomSeed = RandomGenerator.Next();
        }

        public override string ToString() {
            var binary = Pack();

            var result = new StringBuilder();

            int currentIndex = 0;

            foreach (var b in binary) {
                result.Append($"{b:x2}");
                if (currentIndex == 3 || currentIndex == 4 || currentIndex == 8) {
                    result.Append("-");
                }

                currentIndex++;
            }
            
            return result.ToString();
        }

        private byte[] Pack() {
            var result = new List<byte>(10);

            var time = BitConverter.GetBytes(_timeCode);
            result.Add(time[2]);
            result.Add(time[3]);
            result.Add(time[0]);
            result.Add(time[1]);

            var machineId = BitConverter.GetBytes(MachineId);
            result.Add(machineId[0]);

            var rand = BitConverter.GetBytes(_randomSeed);
            result.Add(rand[2]);
            result.Add(rand[3]);
            result.Add(rand[0]);
            result.Add(rand[1]);

            var counter = BitConverter.GetBytes(_myId);
            result.Add(counter[0]);
            
            return result.ToArray();
        }

        public string ToBase32() {
            return Base32.ToBase32String(Pack());
        }
    }
}