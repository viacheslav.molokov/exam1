namespace ShortyNet.Services {
    public interface IShortenedLinkStorage {
        string Encode(string url);
        string? Decode(string url);
    }
}