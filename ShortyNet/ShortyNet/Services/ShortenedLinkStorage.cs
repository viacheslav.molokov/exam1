using System.IO;

namespace ShortyNet.Services {
    public class ShortenedLinkStorage : IShortenedLinkStorage {
        private const string BasePath = "/tmp/links";
        public string Encode(string url) {
            if (!Directory.Exists(BasePath)) {
                Directory.CreateDirectory(BasePath);
            }

            var shortName = new LinkId().ToBase32();
            while (File.Exists(GetFullName(shortName))) {
                shortName = new LinkId().ToBase32();
            }

            File.WriteAllText(GetFullName(shortName), url);
            
            return shortName;
        }

        public string? Decode(string url) {
            if (!File.Exists(GetFullName(url))) {
                return null;
            }

            return File.ReadAllText(GetFullName(url));
        }

        private static string GetFullName(string url) {
            return BasePath + Path.DirectorySeparatorChar + url;
        }
    }
}