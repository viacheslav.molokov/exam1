﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShortyNet.Services;

namespace ShortyNet.Controllers {
    [ApiController]
    [Route("/api/link")]
    public class ShortenerController : ControllerBase {

        private class ShortenedUrl {
            public ShortenedUrl(string url) {
                Url = url;
            }
            public string Url { get; }
        }
        
        private readonly ILogger<ShortenerController> _logger;
        private readonly IShortenedLinkStorage _linkStorage;

        public ShortenerController(ILogger<ShortenerController> logger,
            IShortenedLinkStorage linkStorage) {
            _logger = logger;
            _linkStorage = linkStorage;
        }

        [HttpPost("")]
        [ProducesResponseType(typeof(BadRequestResult), 409)]
        [ProducesResponseType(typeof(CreatedResult), 201)]
        public IActionResult CreateNew([FromForm] string? url) {
            if (string.IsNullOrEmpty(url)) {
                return new BadRequestResult();
            }
            
            var shortenedUrl = _linkStorage.Encode(url);
            return new CreatedResult($"/api/link/{shortenedUrl}", new ShortenedUrl($"/api/link/{shortenedUrl}"));
        }

        [HttpGet("{url}")]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
        [ProducesResponseType(typeof(RedirectResult), 302)]
        public IActionResult Decode(string url) {
            var decodedLink = _linkStorage.Decode(url);
            if (string.IsNullOrEmpty(decodedLink)) {
                return NotFound();
            }

            return Redirect(decodedLink);
        }
    }
}