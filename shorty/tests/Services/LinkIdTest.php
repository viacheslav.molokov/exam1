<?php

namespace App\Tests\Services;

use App\Services\LinkId;
use PHPUnit\Framework\TestCase;

class LinkIdTest extends TestCase
{
    public function testNewId()
    {
        $id = new LinkId();

        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();
        $this->makeLinkId();

        // assert that your calculator added the numbers correctly!
        //$this->assertEquals("42", $result);
    }

    private function makeLinkId() : void
    {
        $id = new LinkId();

        echo $id . "   " . $id->toUrl() . PHP_EOL;
    }
}

?>