<?php

namespace App\Services;

use App\Services\Bin2n;

class LinkId
{
    private int $timeStamp;
    static public int $machineId;
    static public Base2n $base2n;
    private int $randomNumber;
    static private int $nextId = 0;
    private int $myId;
    
    public function __construct()
    {
        if (self::$machineId == null) {
            self::$machineId = rand();
        }

        $this->myId = self::$nextId++ & 0xF;

        $this->timeStamp = time();
        $this->randomNumber = rand();
    }

    public function __toString() : string
    {
        return bin2hex($this->getBinaryForm());
    }

    public function toUrl(): string
    {
        return self::$base2n->encode($this->getBinaryForm());
    }

    private function getBinaryForm() : string
    {
        return pack("SSCLC", ($this->timeStamp >> 16) & 0xFF, $this->timeStamp & 0xFF, self::$machineId, $this->randomNumber, $this->myId);
    }
}

LinkId::$machineId = rand() & 0xF;
LinkId::$base2n = new Base2n(5);

?>