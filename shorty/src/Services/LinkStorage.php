<?php

namespace App\Services;

use App\Services\LinkId;
use Psr\Log\LoggerInterface;

class LinkStorage
{
    private static string $basePath = "/tmp" . DIRECTORY_SEPARATOR . ".links12";
    private LoggerInterface $logger;


    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    public function encode(string $url) : string 
    {
        $linkId = new LinkId();

        while (file_exists($this->getFullName($linkId->toUrl())))
        {
            $linkId = new LinkId();
        }

        if (!file_exists($this->getFullPath($linkId->toUrl())))
        {
            $this->logger->error($this->getFullPath($linkId->toUrl()) . PHP_EOL);
            mkdir($this->getFullPath($linkId->toUrl()), 0x777, true);
            chmod($this->getFullPath($linkId->toUrl()), 0x777);
        }

        file_put_contents($this->getFullName($linkId->toUrl()), $url);
        return $linkId->toUrl();
    }

    public function decode(string $encoded_link) : string
    {
        if (!file_exists($this->getFullPath($encoded_link)))
        {
            $this->logger->error($encoded_link);
            return "";
        }

        return file_get_contents($this->getFullName($encoded_link));
    }

    private function getSubDir(string $link) : string
    {
        return substr($link, 0, 4);
    }

    private function getFullPath(string $link) : string
    {
        return self::$basePath . DIRECTORY_SEPARATOR;// . $this->getSubDir($link). DIRECTORY_SEPARATOR;
    }

    private function getFullName(string $link) : string
    {
        return $this->getFullPath($link)  . $link;
    }

}

?>