<?php

namespace App\Controller;

use App\Services\LinkStorage;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ShortyController extends AbstractController
{
    private LinkStorage $linkStorage;

    public function __construct(LinkStorage $linkStorage) {
        $this->linkStorage = $linkStorage;
    }
    /**
     * @Route("/api/link", methods={"POST"})
     */
    public function create(Request $request) : Response
    {
        $raw_url = $request->request->get('url');
        $link = $this->linkStorage->encode($raw_url);

        $response = new Response('/api/link/' . $link, 201);
        $response->headers->set('Location', '/api/link/' . $link);

        return $response;
    }

    /**
     * @Route("/api/link/{encoded_link}", methods={"GET"})
     */
    public function follow(string $encoded_link) : RedirectResponse
    {
        $decoded_link = $this->linkStorage->decode($encoded_link);

        if (empty($decoded_link))
        {
            throw $this->createNotFoundException("No such URL");
        }

        return new RedirectResponse($decoded_link);
    }
}

?>